package com.epam.rd.java.basic.task7.db;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import com.epam.rd.java.basic.task7.db.entity.*;

public class DBManager {

	private static DBManager instance;
	private final String CONNECTION_URL;
	private final String URL = "connection.url";
	private static final String APP_CONFIG_PATH = "app.properties";

	public static final String GET_USER_BY_LOGIN = "SELECT id, login FROM users WHERE login=?";
	public static final String GET_ALL_USERS = "SELECT id, login FROM users";
	public static final String INSERT_USER = "INSERT INTO users (id, login) VALUES (DEFAULT, ?)";
	public static final String DELETE_USER = "DELETE FROM users WHERE id=? AND login =?";

	public static final String GET_TEAM_BY_NAME = "SELECT id, name FROM teams WHERE name=?";
	public static final String GET_ALL_TEAMS = "SELECT id, name FROM teams";
	public static final String DELETE_TEAM = "DELETE FROM teams WHERE id=? AND name=?";
	public static final String INSERT_TEAM = "INSERT INTO teams (id, name) VALUES (DEFAULT, ?)";
	public static final String UPDATE_TEAM = "UPDATE teams SET name=? WHERE id=?";

	public static final String SET_TEAMS_FOR_USER = "INSERT INTO users_teams (user_id, team_id) VALUES (?, ?)";
	public static final String GET_USER_TEAMS = "SELECT teams.id, teams.name FROM teams " +
						"JOIN users_teams ON teams.id = users_teams.team_id " +
						"WHERE users_teams.user_id = ?";

	private static Connection connection = null;
	private static PreparedStatement preparedStatement = null;
	private static ResultSet resultSet = null;

	public static synchronized DBManager getInstance() {
		if (instance == null) {
			instance = new DBManager();
		}
		return instance;
	}

	private DBManager() {
		CONNECTION_URL = createProperties().getProperty(URL);
	}

	public List<User> findAllUsers() throws DBException {
		List<User> users = new ArrayList<>();
		try {
			Connection connection = DriverManager.getConnection(CONNECTION_URL);
			PreparedStatement statement = connection.prepareStatement(GET_ALL_USERS);
			ResultSet rs = statement.executeQuery();

			while (rs.next()) {
				User user = new User();
				user.setId(rs.getInt("id"));
				user.setLogin(rs.getString("login"));
				users.add(user);

			}
		} catch (SQLException e) {
			throw new DBException("Exception while finding all users", e);
		}

		return users;
	}

	public boolean insertUser(User user) throws DBException {

		try {
			connection = DriverManager.getConnection(CONNECTION_URL);
			preparedStatement = connection.prepareStatement(INSERT_USER, Statement.RETURN_GENERATED_KEYS);
			preparedStatement.setString(1, user.getLogin());

			if (preparedStatement.executeUpdate() > 0) {
				ResultSet resultSet = preparedStatement.getGeneratedKeys();
				if (resultSet.next()) {
					user.setId(resultSet.getInt(1));
					return true;
				}
			}
			return false;

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new DBException("Exception occurred while inserting user", e);
		} finally {
			closeResource(preparedStatement);
			closeResource(connection);
		}
	}

	public boolean deleteUsers(User... users) throws DBException {
		try {
			connection = DriverManager.getConnection(CONNECTION_URL);
			preparedStatement = connection.prepareStatement(DELETE_USER);
			for (User user : users) {
				preparedStatement.setInt(1, user.getId());
				preparedStatement.setString(2, user.getLogin());
				preparedStatement.executeUpdate();
			}
			return true;

		} catch (SQLException e) {
			throw new DBException("Exception occurred while deleting user", e);
		}
	}

	public User getUser(String login) throws DBException {

		try {
			connection = DriverManager.getConnection(CONNECTION_URL);
			preparedStatement = connection.prepareStatement(GET_USER_BY_LOGIN);
			preparedStatement.setString(1, login);
			resultSet = preparedStatement.executeQuery();
			User user = null;
			if (resultSet.next()) {
				user = new User();
				user.setId(resultSet.getInt("id"));
				user.setLogin(resultSet.getString("login"));
			}
			return user;
		} catch (SQLException e) {
			throw new DBException("Exception occurred while getting user by id", e);
		} finally {
			closeResource(resultSet);
			closeResource(preparedStatement);
			closeResource(connection);
		}
	}

	public Team getTeam(String name) throws DBException {
		try {
			connection = DriverManager.getConnection(CONNECTION_URL);
			preparedStatement = connection.prepareStatement(GET_TEAM_BY_NAME);
			preparedStatement.setString(1, name);
			resultSet = preparedStatement.executeQuery();
			Team team = null;
			if (resultSet.next()) {
				team = new Team();
				team.setId(resultSet.getInt("id"));
				team.setName(resultSet.getString("name"));
			}
			return team;

		} catch (SQLException e) {
			throw new DBException("Exception occurred while getting team by name", e);
		} finally {
			closeResource(resultSet);
			closeResource(preparedStatement);
			closeResource(connection);
		}
	}

	public List<Team> findAllTeams() throws DBException {
		List<Team> teams = new ArrayList<>();
		try {
			connection = DriverManager.getConnection(CONNECTION_URL);
			preparedStatement = connection.prepareStatement(GET_ALL_TEAMS);
			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				Team team = new Team();
				team.setId(resultSet.getInt("id"));
				team.setName(resultSet.getString("name"));
				teams.add(team);
			}

		} catch (SQLException e) {
			throw new DBException("Exception while finding new teams", e);
		}

		return teams;
	}

	public boolean insertTeam(Team team) throws DBException {
		try {
			connection = DriverManager.getConnection(CONNECTION_URL);
			preparedStatement = connection.prepareStatement(INSERT_TEAM, Statement.RETURN_GENERATED_KEYS);
			preparedStatement.setString(1, team.getName());

			if (preparedStatement.executeUpdate() > 0) {
				resultSet = preparedStatement.getGeneratedKeys();
				if (resultSet.next()) {
					team.setId(resultSet.getInt(1));
					return true;
				}
			}
			return false;

		} catch (SQLException e) {
			throw new DBException("Exception while inserting teams", e);
		} finally {
			closeResource(resultSet);
			closeResource(preparedStatement);
			closeResource(connection);
		}
	}

	public boolean setTeamsForUser(User user, Team... teams) throws DBException {

		try {
			connection = DriverManager.getConnection(CONNECTION_URL);
			connection.setAutoCommit(false);
			preparedStatement = connection.prepareStatement(SET_TEAMS_FOR_USER);

			for (Team team : teams) {
				preparedStatement.setInt(1, user.getId());
				preparedStatement.setInt(2, team.getId());
				preparedStatement.executeUpdate();
			}

			connection.commit();
			return true;

		} catch (SQLException e) {
			if (connection != null) {
				try {
					connection.rollback();
					throw new DBException("Exception occurred while setting teams for user", e);
				} catch (SQLException ex) {
					throw new DBException("Exception while rolling back transaction", ex);
				}
			}
		} finally {
			closeResource(connection);
			closeResource(preparedStatement);
		}
		return false;
	}

	public List<Team> getUserTeams(User user) throws DBException {
		List<Team> teams = new ArrayList<>();

		try {
			connection = DriverManager.getConnection(CONNECTION_URL);
			preparedStatement = connection.prepareStatement(GET_USER_TEAMS);
			preparedStatement.setInt(1, user.getId());
			resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				Team team = new Team();
				team.setId(resultSet.getInt("id"));
				team.setName(resultSet.getString("name"));
				teams.add(team);
			}

		} catch (SQLException e) {
			throw new DBException("Exception while finding all teams", e);

		} finally {
			closeResource(connection);
			closeResource(preparedStatement);
			closeResource(resultSet);
		}
		return teams;
	}

	public boolean deleteTeam(Team team) throws DBException {
		try {
			connection = DriverManager.getConnection(CONNECTION_URL);
			preparedStatement = connection.prepareStatement(DELETE_TEAM);
			preparedStatement.setInt(1, team.getId());
			preparedStatement.setString(2, team.getName());
			return preparedStatement.executeUpdate() > 0;
		} catch (SQLException e) {
			throw new DBException("Exception occurred while deleting team", e);
		}
	}

	public boolean updateTeam(Team team) throws DBException {
		try {
			connection = DriverManager.getConnection(CONNECTION_URL);
			preparedStatement = connection.prepareStatement(UPDATE_TEAM);
			preparedStatement.setInt(2, team.getId());
			preparedStatement.setString(1, team.getName());
			return preparedStatement.executeUpdate() > 0;

		} catch (SQLException e) {
			throw new DBException("Exception occurred while updating team", e);
		}
	}

	private Properties createProperties() {
		Properties appProps = new Properties();
		try (var input = new FileInputStream(APP_CONFIG_PATH)) {
			appProps.load(input);
		} catch (IOException exception) {
			exception.printStackTrace();
		}
		return appProps;
	}

	private void closeResource(AutoCloseable autoCloseable) {
		if (autoCloseable != null) {
			try {
				autoCloseable.close();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
	}
}
